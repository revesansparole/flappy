class Bird:

    def __init__(self, loc):
        self._y = loc
        self._size = 0.03
        self._up_speed = 0.3
        self._down_speed = 0.3

    def loc(self):
        return self._y

    def size(self):
        return self._size

    def speeds(self):
        return self._up_speed, self._down_speed

    def fly(self, up, dt):
        """New bird position over dt interval

        Args:
            up (float): [s] time up key has been pushed
            dt (float): [s] time interval

        Returns:
            None
        """
        up = max(0., min(up, dt))
        new_y = self._y + up * self._up_speed - (dt - up) * self._down_speed

        self._y = max(0., min(1., new_y))
