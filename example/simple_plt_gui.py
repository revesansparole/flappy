import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from flappy.arena import Arena

dt = 0.2
arena = Arena()
awidth, aheight = arena.size()

fig, ax = plt.subplots(1, 1, figsize=(8, 7 * aheight / awidth), squeeze=True)
img_bird, = plt.plot([], [], '-r')
img_gates = []
for i in range(4):
    crv, = plt.plot([], [], '-b')
    img_gates.append(crv)

nb_clicks = [0]


def init():
    ax.set_xlim(-0.1, awidth)
    ax.set_ylim(0, aheight)
    ax.set_xticks([])
    ax.set_yticks([])
    fig.tight_layout()
    return [img_bird] + img_gates


def update(frame):
    print("fr", frame)
    up = nb_clicks[0] * 0.1
    nb_clicks[0] = 0

    arena.loop(up, dt)
    if arena.is_collision():
        bird_loc = aheight / 2
        bird_size = 0.1
        img_bird.set_data([-bird_size * 2, -bird_size * 2, 0, 0, -bird_size * 2],
                          [bird_loc - bird_size, bird_loc + bird_size, bird_loc + bird_size, bird_loc - bird_size,
                           bird_loc - bird_size])
        return img_bird,

    # draw bird
    bird_loc = arena.bird().loc()
    bird_size = arena.bird().size()
    img_bird.set_data([-bird_size * 2, -bird_size * 2, 0, 0, -bird_size * 2],
                      [bird_loc - bird_size, bird_loc + bird_size, bird_loc + bird_size, bird_loc - bird_size,
                       bird_loc - bird_size])

    # draw gates
    updated_gates = []
    for gate, crv_gate in zip(arena.gates(), img_gates):
        x = gate.loc()
        bot, top, width = gate.size()
        crv_gate.set_data([x, x, x + width, x + width, np.nan, x, x, x + width, x + width],
                          [0, bot, bot, 0, np.nan, aheight, top, top, aheight])
        updated_gates.append(crv_gate)

    return [img_bird] + updated_gates


def onclick(event):
    if event.dblclick:
        nb_clicks[0] += 2
    else:
        nb_clicks[0] += 1
    print("click")


cid = fig.canvas.mpl_connect('button_press_event', onclick)

ani = FuncAnimation(fig, update, frames=200, init_func=init, blit=True, repeat=FuncAnimation, interval=200)

plt.show()
