# {# pkglts, reqs
# requirements are managed by pkglts, do not edit this file at all
# edit .pkglts/pkg_cfg instead
# section reqs

# doc
sphinx


# dvlpt
twine


# install
matplotlib
numpy


# test
coverage
pytest
pytest-cov
pytest-mock


# #}
