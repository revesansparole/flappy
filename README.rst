========================
flappy
========================

.. {# pkglts, doc


.. image:: https://badge.fury.io/py/flappy.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/flappy
.. #}

develop: |dvlp_build|_ |dvlp_coverage|_

.. |dvlp_build| image:: https://gitlab.com/revesansparole/flappy/badges/develop/build.svg
.. _dvlp_build: https://gitlab.com/revesansparole/flappy/commits/develop

.. |dvlp_coverage| image:: https://gitlab.com/revesansparole/flappy/badges/develop/coverage.svg
.. _dvlp_coverage: https://gitlab.com/revesansparole/flappy/commits/develop


master: |master_build|_ |master_coverage|_

.. |master_build| image:: https://gitlab.com/revesansparole/flappy/badges/master/build.svg
.. _master_build: https://gitlab.com/revesansparole/flappy/commits/master

.. |master_coverage| image:: https://gitlab.com/revesansparole/flappy/badges/master/coverage.svg
.. _master_coverage: https://gitlab.com/revesansparole/flappy/commits/master

Flappy bird in python for fun and to learn AI

