class Gate:

    def __init__(self, bot=0.4, top=0.6, width=0.2):
        self._x = 1
        self._bot = bot
        self._top = top
        self._width = width
        self._speed = 0.1

    def loc(self):
        return self._x

    def size(self):
        return self._bot, self._top, self._width

    def width(self):
        return self._width

    def move(self, dt):
        self._x -= self._speed * dt
