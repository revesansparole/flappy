from random import uniform

from .bird import Bird
from .gate import Gate


class Arena:

    def __init__(self):
        self._width = 1.
        self._height = 1.
        self._bird = Bird(self._height / 2.)
        self._gates = []
        self.create_gate()

    def size(self):
        return self._width, self._height

    def bird(self):
        return self._bird

    def gates(self):
        return self._gates

    def create_gate(self):
        if len(self._gates) == 0 or self._gates[-1].loc() < 0.5:
            height = 0.2  # make it a function of time
            bot = uniform(0, self._height - height)
            top = bot + height
            self._gates.append(Gate(bot, top))

    def cleanup_gates(self):
        self._gates = [g for g in self._gates if g.loc() > -g.width()]

    def status(self):
        st = f"bird: {self._bird.loc()}\n"
        for i, gate in enumerate(self._gates):
            st += f"gate{i:d}: {gate.loc():.2f} {gate.size()}\n"

        return st

    def is_collision(self):
        """Check whether bird intersect gate

        Returns:
            (bool)
        """
        if len(self._gates) == 0:
            return False

        gate = self._gates[0]  # no reason to consider other gates
        bot, top, width = gate.size()
        bird_loc = self._bird.loc()
        bird_size = self._bird.size()
        if -width <= gate.loc() <= 0 and ((bird_loc - bird_size) <= bot or ((bird_loc + bird_size) >= top)):
            return True

        return False

    def loop(self, up, dt):
        """Evolve arena by dt interval.

        Args:
            up (float): [s] time up key has been pushed
            dt (float): [s] time interval

        Returns:
            (bool): True if no collision occurred
        """
        self._bird.fly(up, dt)
        for gate in self._gates:
            gate.move(dt)

        if self.is_collision():
            return False

        self.create_gate()
        self.cleanup_gates()

        return True

    def assess_situation(self):
        """Estimate situation from bird point of view

        Returns:
            (tuple):
             - (float): horizontal distance to nearest gate (0 if still passing a gate)
             - (float): vertical distance to bottom of nearest gate (positive up)
             - (float): vertical distance to top of nearest gate (positive up)
        """
        if len(self._gates) == 0:
            return self._width, 0, self._height

        gate = self._gates[0]
        bot, top, width = gate.size()
        bird_loc = self._bird.loc()

        return max(0, gate.loc()), bot - bird_loc, top - bird_loc
