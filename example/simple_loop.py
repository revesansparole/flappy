from flappy.arena import Arena

dt = 1.
arena = Arena()

for i in range(200):
    print(i, arena.status())
    dh, dbot, dtop = arena.assess_situation()

    # myalgo to win the game
    up_speed, down_speed = arena.bird().speeds()
    up = ((dbot + dtop) / 2 + dt * down_speed) / (up_speed + down_speed)

    if not arena.loop(up, dt):
        raise UserWarning("end of game")

print("end", arena.status())
